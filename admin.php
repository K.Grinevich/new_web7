<?php

/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 **/

// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.

$user = 'u26391';
$pass = '7979685';
$db = new PDO('mysql:host=localhost;dbname=u26391', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
$sel = $db->query("SELECT login FROM Admin");
    foreach($sel as $el)
      $login=$el['login'];
  $sel = $db->query("SELECT pass FROM Admin");
    foreach($sel as $el)
      $pas=$el['pass'];

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $messages = array();

  $errors = array();
  $errors['error'] = !empty($_COOKIE['er_error']);
  if ($errors['error']) {    
    setcookie('er_error', '', 100000);
    $messages[] = '<div class="error">id редактируемого и id удаляемого пользователя не могут совпадать.</div>';
  }

if (empty($_SERVER['PHP_AUTH_USER']) ||
    empty($_SERVER['PHP_AUTH_PW']) ||
    $_SERVER['PHP_AUTH_USER'] != $login ||
    md5($_SERVER['PHP_AUTH_PW']) != $pas) {
  header('HTTP/1.1 401 Unanthorized');
  header('WWW-Authenticate: Basic realm="My site"');
  print('<h1>401 Требуется авторизация</h1>');
  exit();
}
print('Вы успешно авторизовались и видите защищенные паролем данные.');

$res=$db->query("SELECT * FROM application2");
$sp1=$db->query("SELECT count(*) from spw where nom_spw = 1");
$sp2=$db->query("SELECT count(*) from spw where nom_spw = 2");
$sp3=$db->query("SELECT count(*) from spw where nom_spw = 3");

  ?>
  <style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
.error {
  border: 2px solid red;
}
    </style>
 
<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>
<h2>Информация о пользователях</h2>
  <table border="2">
    <tr>
      <td>ID</td>
      <td>ФИО</td>
      <td>E-mail</td>
      <td>Год рождения</td>
      <td>Пол</td>
      <td>Кол-во конечностей</td>
      <td>Биография</td>
    </tr>
  <?php foreach($res as $el){ ?>
    <tr>
      <td><?php print($el['id']); ?></td>
      <td><?php print($el['fio']); ?></td>
      <td><?php print($el['email']); ?></td>
      <td><?php print($el['yob']); ?></td>
      <td><?php print($el['pol']); ?></td>        
      <td><?php print($el['limb']); ?></td>
      <td><?php print($el['biography']); ?></td>
      <?php } ?>
    </tr>
  </table>
  <h2>Статистика по суперспособностям</h2>
  <table border="2">
    <tr>
      <td>Бессмертие</td>
      <td>Прохождение сквозь стены</td>
      <td>Левитация</td>
    </tr>
     
    <tr>
      <td><?php foreach($sp1 as $el)
      print($el['count(*)']);
       ?></td>
      <td><?php foreach($sp2 as $el)
      print($el['count(*)']); ?></td>
      <td><?php foreach($sp3 as $el)
      print($el['count(*)']); ?></td>
    </tr>
  </table>

  <br/>

  <form action="" method="post">
    Введите id для редактирования
    <br/>
    <input name="edit" /> 
    <br/><br/>
    Введите id для удаления
    <br/>
    <input name="del" /> 
    <br/><br/>
    <input type="submit" value="Подтвердить" />
  </form>
  <?php


}else
{
  $errors = FALSE;
  if($_POST['edit']==$_POST['del']) {
    setcookie('er_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }

    if ($errors) {
      header('Location: admin.php');
      exit();
    }
    else {
      setcookie('er_error', '', 100000);
    }

    if (!empty($_POST['del'])){
      $idd=(int)$_POST['del'];
      $db->query("DELETE FROM spw WHERE id = $idd");
      $db->query("DELETE FROM l_p WHERE id = $idd");
      $db->query("DELETE FROM application2 WHERE id = $idd");
      header('Location: admin.php');
    }


    if (!empty($_POST['edit'])){
    session_start();
    $_SESSION['uid']=(int)$_POST['edit'];
    $_SESSION['login']='Admin';
    header('Location: ./');
    }

}

// *********
// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.
// *********